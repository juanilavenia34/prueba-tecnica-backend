﻿namespace Test_1.Model
{
    public class ClientModel
    {
        public string RazonSocial { get; set; }
        public string Cuit { get; set; }
        public bool Posnet { get; set; }
    }
}
