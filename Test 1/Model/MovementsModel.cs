﻿namespace Test_1.Model
{
    public class MovementsModel
    {
        public int IdCA { get; set; }
        public string NumReceipt { get; set; }
        public DateTime DateReceipt { get; set; }
        public string ReceiptType { get; set; }
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public int IdClient { get; set; }
    }
}
