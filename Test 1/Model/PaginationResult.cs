﻿namespace Test_1.Model
{
    public class PaginationResult<T>
    {
        public int TotalItems { get; set; }
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public List<T> Items { get; set; }
    }
}
