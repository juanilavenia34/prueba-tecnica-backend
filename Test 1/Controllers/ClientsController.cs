﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PruebaTecnicaBackend.Data.Repositories;
using PruebaTecnicaBackend.Model;
using Test_1.Model;

namespace PruebaTecnicaBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        private readonly IClientRepository _clientRepository;

        public ClientsController(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }
        [HttpGet]
        public async Task<ActionResult<PaginationResult<Client>>> GetAllClients(int page = 1, int limit = 5)
        {
            if (page < 1) page = 1;
            if (limit < 1) limit = 10;

          
            var allClients = await _clientRepository.GetAllClients();

            int totalClients = allClients.Count;
            int startIndex = (page - 1) * limit;

            
            var clients = allClients.Skip(startIndex).Take(limit).ToList();

            var paginationResult = new PaginationResult<Client>
            {
                TotalItems = totalClients,
                CurrentPage = page,
                ItemsPerPage = limit,
                Items = clients
            };

            return Ok(paginationResult);
        }



        [HttpGet("{id}", Name = "GetClient")]
        public async Task<ActionResult<Client>> GetById(int id)
        {
            var client = await _clientRepository.GetClientById(id);
            if (client == null)
            {
                return NotFound("No se encontro el cliente");
            }
            return client;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]ClientModel client)
        {
            var newClient = await _clientRepository.AddClient(client.RazonSocial, client.Cuit, client.Posnet);
            if(newClient == null)
            {
                return NotFound("El cuit del cliente ya esta registrado");
            }
            return Ok(newClient);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateClient(int id, ClientModel client)
        {
            var existingClient = await _clientRepository.GetClientById(id);
            if (existingClient == null)
            {
                return NotFound("No se encontró el cliente");
            }
            existingClient.Cuit = client.Cuit;
            existingClient.Posnet = client.Posnet;
            existingClient.RazonSocial = client.RazonSocial;

            await _clientRepository.UpdateClient(existingClient);
            return Ok("Cliente modificado correctamente");
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var client = await _clientRepository.GetClientById(id);
            if (client == null)
            {
                return NotFound();
            }

            await _clientRepository.DeleteClient(id);
            return Ok("Cliente eliminado correctamente");
        }

    }
}
