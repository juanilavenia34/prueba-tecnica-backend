﻿using QuestPDF.Fluent;
using QuestPDF.Helpers;
using QuestPDF.Previewer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PruebaTecnicaBackend.Data.Repositories;
using PruebaTecnicaBackend.Model;
using Test_1.Model;

namespace Test_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrentAccountController : ControllerBase
    {
        private readonly ICurrentAccountRepository _repository;
        private readonly ILogger<CurrentAccountController> _logger;

        public CurrentAccountController(ICurrentAccountRepository repository, ILogger<CurrentAccountController> logger)
        {
            _repository = repository;
            _logger = logger;
        }


        [HttpGet]
        public async Task<ActionResult<List<CurrentAccount>>> Get(string clientData)
        {
            var currentAccount = new List<CurrentAccount>();
            var isInteger= int.TryParse(clientData, out int n);
            if (isInteger)
            {
                var intData= Int32.Parse(clientData); 
                currentAccount = await _repository.GetDetailsById(intData);
            }

            else
            {
                 currentAccount = await _repository.GetDetailsByName(clientData);
            }

            if (currentAccount == null)
            {
                return NotFound();
            }

            return currentAccount;
        }

        [HttpPost]
        public IActionResult GenerarPdf([FromBody] List<MovementsModel> movements)
        {
            try
            {
                var data = Document.Create(document =>
                {
                    document.Page(page =>
                    {
                        page.Margin(30);

                        page.Header().ShowOnce().Row(row =>
                        {


                            row.RelativeItem().Column(col =>
                            {
                                col.Item().AlignCenter().Text("Trocadero Distribuidora").Bold().FontSize(14);
                                col.Item().AlignCenter().Text("Pedro Pascal 375 - Tucumán").FontSize(9);
                                col.Item().AlignCenter().Text("381 5116845 / 011 6991458").FontSize(9);
                                col.Item().AlignCenter().Text("trocadero@distribuidora.com").FontSize(9);

                            });

                            row.RelativeItem().Column(col =>
                            {
                                col.Item().Border(1).BorderColor("#257272")
                                .AlignCenter().Text("RUC 21312312312");

                                col.Item().Background("#257272").Border(1)
                                .BorderColor("#257272").AlignCenter()
                                .Text("Reporte de cuenta").FontColor("#fff");

                                col.Item().Border(1).BorderColor("#257272").
                                AlignCenter().Text("B0001 - 001");


                            });
                        });

                        page.Content().PaddingVertical(10).Column(col1 =>
                        {
                            col1.Item().Column(col2 =>
                            {
                                col2.Item().Text("Datos del cliente").Underline().Bold();

                                col2.Item().Text(txt =>
                                {
                                    txt.Span("Razon Social: ").SemiBold().FontSize(10);
                                    txt.Span("XXXXXXXXX").FontSize(10);
                                });

                                col2.Item().Text(txt =>
                                {
                                    txt.Span("CUIT: ").SemiBold().FontSize(10);
                                    txt.Span("30-XXXXXXXX-X").FontSize(10);
                                });

                                col2.Item().Text(txt =>
                                {
                                    txt.Span("Direccion: ").SemiBold().FontSize(10);
                                    txt.Span("XXXXXXXXXXX").FontSize(10);
                                });
                            });

                            col1.Item().LineHorizontal(0.5f);

                            col1.Item().Table(tabla =>
                            {
                                tabla.ColumnsDefinition(columns =>
                                {
                                    columns.RelativeColumn(2); 
                                    columns.RelativeColumn(2); 
                                    columns.RelativeColumn(2); 
                                    columns.RelativeColumn();   
                                    columns.RelativeColumn();   
                                });

                                tabla.Header(header =>
                                {
                                    header.Cell().Background("#257272").Padding(2).Text("Fecha del Comprobante").FontColor("#fff");
                                    header.Cell().Background("#257272").Padding(2).Text("Nro del Comprobante").FontColor("#fff");
                                    header.Cell().Background("#257272").Padding(2).Text("Tipo de Comprobante").FontColor("#fff");
                                    header.Cell().Background("#257272").Padding(2).Text("Tipo").FontColor("#fff");
                                    header.Cell().Background("#257272").Padding(2).Text("Monto").FontColor("#fff");
                                });

                                foreach (var movement in movements)
                                {
                                    var fechaComprobante = movement.DateReceipt.ToString("yyyy-MM-dd");
                                    var nroComprobante = movement.NumReceipt;
                                    var tipoComprobante = movement.ReceiptType;
                                    var tipo = movement.Type;
                                    var monto = movement.Amount.ToString();

                                    tabla.Cell().BorderBottom(0.5f).BorderColor("#D9D9D9").Padding(2).Text(fechaComprobante).FontSize(10);
                                    tabla.Cell().BorderBottom(0.5f).BorderColor("#D9D9D9").Padding(2).Text(nroComprobante).FontSize(10);
                                    tabla.Cell().BorderBottom(0.5f).BorderColor("#D9D9D9").Padding(2).Text(tipoComprobante).FontSize(10);
                                    tabla.Cell().BorderBottom(0.5f).BorderColor("#D9D9D9").Padding(2).Text(tipo).FontSize(10);
                                    tabla.Cell().BorderBottom(0.5f).BorderColor("#D9D9D9").Padding(2).Text($"${monto}").FontSize(10);
                                }


                            });

                            col1.Item().AlignRight().Text("Total: XXXXX").FontSize(12);

                        });


                        page.Footer()
                        .AlignRight()
                        .Text(txt =>
                        {
                            txt.Span("Pagina ").FontSize(10);
                            txt.CurrentPageNumber().FontSize(10);
                            txt.Span(" de ").FontSize(10);
                            txt.TotalPages().FontSize(10);
                        });
                    });
                }).GeneratePdf();

                Stream stream = new MemoryStream(data);
                return File(stream, "application/pdf", "reportedetabla.pdf");

            
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error al generar el PDF.");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error al generar el PDF.");
            }
        }

        [HttpGet("GetByDateRange")]
        public async Task<ActionResult<List<CurrentAccount>>> GetByDateRange(string startDate, string endDate)
        {
            try
            {
                
                DateTime? parsedStartDate = null;
                DateTime? parsedEndDate = null;

                if (!string.IsNullOrEmpty(startDate) && DateTime.TryParse(startDate, out DateTime tempStartDate))
                {
                    parsedStartDate = tempStartDate;
                }

                if (!string.IsNullOrEmpty(endDate) && DateTime.TryParse(endDate, out DateTime tempEndDate))
                {
                    parsedEndDate = tempEndDate;
                }

                
                var currentAccount = await _repository.GetDetailsByDateRange(parsedStartDate, parsedEndDate);

                if (currentAccount == null)
                {
                    return NotFound();
                }

                return currentAccount;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error al obtener los detalles de la cuenta corriente por rango de fechas.");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error al obtener los detalles de la cuenta corriente por rango de fechas.");
            }
        }
    }
}
