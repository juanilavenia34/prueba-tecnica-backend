﻿using BCrypt.Net;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using PruebaTecnicaBackend.Data;
using PruebaTecnicaBackend.Data.Repositories;
using PruebaTecnicaBackend.Model;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Test_1.Model;

namespace PruebaTecnicaBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly DatabaseContext _context;
        public UserController(IConfiguration configuration, DatabaseContext context)
        {
            _configuration = configuration;
            _context = context;
        }

        [HttpPost("login")]
        public IActionResult Login([FromForm] LoginModel model)
        {
            var userLocated = _context.User.FirstOrDefault(u =>
                u.Username == model.Username);

            if (userLocated == null || !BCrypt.Net.BCrypt.Verify(model.Password, userLocated.Password))
            {
                return Unauthorized();
            }

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, model.Username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: _configuration["Jwt:Issuer"],
                audience: _configuration["Jwt:Audience"],
                claims: claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials);

            var response = new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token)
            };

            return Ok(response);
        }
        [HttpPost("register")]
        public IActionResult Register(User user)
        {
            
            if (_context.User.Any(u => u.Username == user.Username))
            {
                return BadRequest(new { message = "El nombre de usuario ya está en uso" });
            }

            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);

            _context.User.Add(user);
            _context.SaveChanges();

            return Ok(new { message = "Usuario registrado con éxito" });
        }
    }
}
