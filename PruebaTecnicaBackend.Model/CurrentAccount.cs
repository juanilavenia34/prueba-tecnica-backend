﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnicaBackend.Model
{
    public class CurrentAccount
    {
        [Key]
        public int IdCA { get; set; }
        public string NumReceipt { get; set; }
        public DateTime DateReceipt { get; set; }
        public string ReceiptType { get; set; }
        public string Type { get; set; }
        public double Amount { get; set; }
        public int IdClient { get; set; }
    }
}
