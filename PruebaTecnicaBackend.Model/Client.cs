﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnicaBackend.Model
{
    public class Client
    {
        [Key]
        public int IdClient { get; set; }
        public string RazonSocial { get; set; }
        public string Cuit { get; set; }
        public bool Posnet { get; set; }
    }
}
