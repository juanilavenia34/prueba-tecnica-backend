﻿using PruebaTecnicaBackend.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnicaBackend.Data.Repositories
{
    public interface ICurrentAccountRepository
    {
        Task<List<CurrentAccount>> GetDetailsById(int idClient);
        Task<List<CurrentAccount>> GetDetailsByName(string clientName);
        Task<List<CurrentAccount>> GetDetailsByDateRange(DateTime? startDate, DateTime? endDate);
    }
}
