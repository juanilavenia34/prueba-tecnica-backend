﻿using BCrypt.Net;
using MySql.Data.MySqlClient;
using PruebaTecnicaBackend.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnicaBackend.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly MySqlConfig _connectionString;
        public UserRepository(MySqlConfig connectionString)
        {
            _connectionString = connectionString;
        }
        protected MySqlConnection dbConnection()
        {
            return new MySqlConnection(_connectionString.ConnectionString);
        }
        public async Task<User?> Login(string username, string password)
        {
            using (var conn = dbConnection())
            {
                conn.Open();
                var cmd = new MySqlCommand(
                    "SELECT * FROM Users WHERE Username = @username",
                    conn);
                cmd.Parameters.AddWithValue("@username", username);
                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    if (await reader.ReadAsync())
                    {
                        var hashedPassword = reader.GetString("Password");
                        if (!BCrypt.Net.BCrypt.Verify(password, hashedPassword))
                        {
                            return null;
                        }

                        return new User
                        {
                            IdUser = reader.GetInt32("IdUser"),
                            Name = reader.GetString("Name"),
                            Username = reader.GetString("Username")
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public async Task<User?> Register(string name, string username, string password)
        {
            using (var conn = dbConnection())
            {
                conn.Open();
                var cmd = new MySqlCommand(
                    "INSERT INTO Users (Name, Username, Password) VALUES (@name, @username, @password)",
                    conn);
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@password", password);
                await cmd.ExecuteNonQueryAsync();
                return new User { Name = name, Username = username };
            }
        }
    }
}
