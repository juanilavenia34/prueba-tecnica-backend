﻿using PruebaTecnicaBackend.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnicaBackend.Data.Repositories
{
    public interface IClientRepository
    {
        Task<List<Client>> GetAllClients();
        Task<List<Client>> GetClientsPaginated(int startIndex, int take);
        Task<List<Client>> GetPosnetClient(bool posnet);
        Task<Client?> GetClientById(int id);
        Task<Client> UpdateClient(Client client);
        Task<Client> AddClient(string razonSocial, string cuit, bool posnet);
        Task<Client> DeleteClient(int id);
    }
}
