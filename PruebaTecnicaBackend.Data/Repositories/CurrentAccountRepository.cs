﻿using Microsoft.EntityFrameworkCore;
using PruebaTecnicaBackend.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnicaBackend.Data.Repositories
{
    public class CurrentAccountRepository : ICurrentAccountRepository
    {
        private readonly DatabaseContext _context;

        public CurrentAccountRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async Task<List<CurrentAccount>> GetDetailsById(int idClient)
        {
            return await _context.CurrentAccount
                .Where(ca => ca.IdClient == idClient)
                .ToListAsync();
        }
        public async Task<List<CurrentAccount>> GetDetailsByName(string clientName)
        {
            return await (from ca in _context.CurrentAccount
                          join cli in _context.Client on ca.IdClient equals cli.IdClient
                          where cli.RazonSocial == clientName
                          select ca ).ToListAsync();
            
        }
        public async Task<List<CurrentAccount>> GetDetailsByDateRange(DateTime? startDate, DateTime? endDate)
        {
            if (startDate.HasValue && endDate.HasValue)
            {
                return await _context.CurrentAccount
                    .Where(ca => ca.DateReceipt >= startDate.Value && ca.DateReceipt <= endDate.Value)
                    .ToListAsync();
            }
            else if (startDate.HasValue)
            {
                return await _context.CurrentAccount
                    .Where(ca => ca.DateReceipt >= startDate.Value)
                    .ToListAsync();
            }
            else if (endDate.HasValue)
            {
                return await _context.CurrentAccount
                    .Where(ca => ca.DateReceipt <= endDate.Value)
                    .ToListAsync();
            }
            else
            {
                // If no date range is provided, return all details
                return await _context.CurrentAccount.ToListAsync();
            }
        }

    }

}
