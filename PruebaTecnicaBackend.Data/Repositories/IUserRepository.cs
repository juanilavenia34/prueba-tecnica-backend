﻿using PruebaTecnicaBackend.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnicaBackend.Data.Repositories
{
    public interface IUserRepository
    {
        Task<User?> Login(string username, string password);
        Task<User?> Register(string name, string username, string password);
    }
}
