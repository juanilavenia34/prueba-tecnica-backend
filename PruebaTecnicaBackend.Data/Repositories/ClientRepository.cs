﻿using Microsoft.EntityFrameworkCore;
using PruebaTecnicaBackend.Data;
using PruebaTecnicaBackend.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnicaBackend.Data.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private readonly DatabaseContext _contexto;

        public ClientRepository(DatabaseContext contexto)
        {
            _contexto = contexto;
        }

        public async Task<List<Client>> GetAllClients()
        {
            return await _contexto.Client.ToListAsync();
        }

        public async Task<List<Client>> GetClientsPaginated(int startIndex, int take)
        {
            
            var clients = await _contexto.Client
                                    .OrderBy(c => c.IdClient)
                                    .Skip(startIndex)
                                    .Take(take)
                                    .ToListAsync();

            return clients;
        }


        public async Task<List<Client>> GetPosnetClient(bool posnet)
        {
            return await _contexto.Client.Where(c => c.Posnet == posnet).ToListAsync();
        }

        public async Task<Client?> GetClientById(int id)
        {
            return await _contexto.Client.FindAsync(id);
        }

        public async Task<Client> UpdateClient(Client client)
        {
            _contexto.Client.Update(client);
            await _contexto.SaveChangesAsync();
            return client;
        }

        public async Task<Client> AddClient(string razonSocial, string cuit, bool posnet)
        {
            try
            {
               var client = new Client
            {
                RazonSocial = razonSocial,
                Cuit = cuit,
                Posnet = posnet
            };
            _contexto.Client.Add(client);
            await _contexto.SaveChangesAsync();
            return client;
            } catch (Exception ex)
            {
                return null;
            }
            
        }

        public async Task<Client> DeleteClient(int id)
        {
            var client = await _contexto.Client.FindAsync(id);
            if (client != null)
            {
                _contexto.Client.Remove(client);
                await _contexto.SaveChangesAsync();
                return client;
            }
            return null; 
        }
    }
}
